<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/projects', 'ProjectsController@apiProjects');

Route::group(['prefix' => 'tasks'], function () {
    Route::get('/', 'TasksController@apiTasks');
    Route::post('/update/{id}', 'TasksController@update');
    Route::delete('/delete/{id}', 'TasksController@destroy');
});
