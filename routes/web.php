<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
    Route::get('/projects', 'ProjectsController@index')->name('home');
    Route::get('/projects/create', 'ProjectsController@create')->name('projects.create');
    Route::post('/projects', 'ProjectsController@store')->name('projects.store');

    Route::group(['prefix' => 'tasks'], function () {
        Route::get('/', 'TasksController@index')->name('tasks');
        Route::get('/create', 'TasksController@create')->name('tasks.create');
        Route::post('/', 'TasksController@store')->name('tasks.store');
        Route::get('/edit/{id}', 'TasksController@edit')->name('tasks.edit');
        Route::post('/update/{id}', 'TasksController@update')->name('tasks.update');
    });
});

