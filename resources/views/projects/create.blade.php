@extends('layouts.app')

@section('content')
    {!! Form::open(['route' => 'projects.store']) !!}

    <div class="form-group">
        {!! Form::label('label', 'Name') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    {!! Form::submit('Create Project', ['class' => 'btn btn-success']) !!}
    {!! Form::close() !!}
@endsection