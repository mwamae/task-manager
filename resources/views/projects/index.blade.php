@extends('layouts.app')

@section('content')
    <p>
        <a  href="{{ route('projects.create') }}">Create Project</a>
    </p>
   <table class="table table-bordered">
       <tr class="thead-light">
           <th>Project Name</th>
       </tr>
       @foreach($projects as $project)
           <tr>
               <td><a href="#">{!! $project->name !!}</a></td>
           </tr>
       @endforeach
   </table>
@endsection