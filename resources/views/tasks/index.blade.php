@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{ route('tasks.create') }}">Create a Task</a>
    </div>
    <tasks></tasks>
@endsection