@extends('layouts.app')

@section('content')
    {!! Form::open(['route' => 'tasks.store']) !!}

    @include('tasks._form')
    {!! Form::submit('Create Task', ['class' => 'btn btn-success']) !!}
    {!! Form::close() !!}
@endsection