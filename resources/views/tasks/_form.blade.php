<div class="form-group">
    {!! Form::label('label', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('label', 'Project') !!}
    {!! Form::select('project_id', $projects, null, ['class' => 'form-control']) !!}
</div>