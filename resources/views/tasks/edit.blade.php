@extends('layouts.app')

@section('content')
    {!! Form::model($task, ['url' => '/dashboard/tasks/update/' . $task->id]) !!}

    @include('tasks._form')
    {!! Form::submit('Update Task', ['class' => 'btn btn-success']) !!}
    {!! Form::close() !!}
@endsection