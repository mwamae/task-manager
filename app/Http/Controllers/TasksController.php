<?php

namespace App\Http\Controllers;

use App\Projects\ProjectRepository;
use App\Projects\Tasks\TasksRepository;
use Illuminate\Http\Request;

class TasksController extends Controller
{

    private $tasksRepository;
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * TasksController constructor.
     */
    public function __construct(TasksRepository $tasksRepository, ProjectRepository $projectRepository)
    {
        $this->tasksRepository = $tasksRepository;
        $this->projectRepository = $projectRepository;
    }

    public function create()
    {
        $projects = $this->projectRepository->lists();

        return view('tasks.create', compact('projects'));
    }

    public function store(Request $request)
    {
        $input = $request->only(['project_id', 'name']);

        $this->tasksRepository->save($input);

        return redirect()->route('tasks');
    }

    public function index()
    {
        return view('tasks.index');
    }

    public function edit($id)
    {
        $task = $this->tasksRepository->getById($id);
        $projects = $this->projectRepository->lists();

        return view('tasks.edit', compact('task', 'projects'));
    }

    public function apiTasks()
    {
        return $this->tasksRepository->all();
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->tasksRepository->update($id, $input);

        return view('tasks.index');
    }

    public function destroy(Request $request, $id)
    {
        return $this->tasksRepository->remove($id);
    }
}
