<?php

namespace App\Http\Controllers;

use App\Projects\ProjectRepository;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{

    private $projectRepository;

    /**
     * ProjectsController constructor.
     */
    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $this->projectRepository->save($input);

        return redirect()->route('home');
    }

    public function index()
    {
        $projects = $this->projectRepository->all();

        return view('projects.index', compact('projects'));
    }

    public function apiProjects()
    {
        return $this->projectRepository->all();
    }
}
