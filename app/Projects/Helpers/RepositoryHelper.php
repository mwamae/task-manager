<?php
/**
 * Created by PhpStorm.
 * User: mwamae
 * Date: 2020-07-04
 * Time: 19:18
 */

namespace App\Projects\Helpers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RepositoryHelper
{
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function save($array)
    {
        $model = $this->model->fill($array);
        $model->save();
        return $model;
    }

    public function update($id, $array)
    {
        $update = $this->getById($id);
        $update->update($array);
        return $update;
    }

    public function remove($id)
    {
        $update = $this->getById($id);
        $update->delete();
        return $update;
    }

    public function getById($id)
    {
        $model = $this->model->where('id', $id)->first();
        if (is_null($model)) {
            throw new ModelNotFoundException;
        }
        return $model;
    }

    public function all()
    {
        return $this->model->get();
    }
}