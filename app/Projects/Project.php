<?php

namespace App\Projects;

use App\Projects\Tasks\Task;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $guarded = ['id'];

    public function tasks()
    {
        return $this->hasMany(Task::class, 'project_id');
    }
}
