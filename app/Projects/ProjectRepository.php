<?php
/**
 * Created by PhpStorm.
 * User: mwamae
 * Date: 2020-07-04
 * Time: 19:17
 */

namespace App\Projects;


use App\Projects\Helpers\RepositoryHelper;

class ProjectRepository extends RepositoryHelper
{
    public function __construct()
    {
        parent::__construct(new Project());
    }

    public function lists()
    {
        return Project::pluck('name', 'id');
    }
}