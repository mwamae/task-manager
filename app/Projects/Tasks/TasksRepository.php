<?php
/**
 * Created by PhpStorm.
 * User: mwamae
 * Date: 2020-07-04
 * Time: 19:17
 */

namespace App\Projects\Tasks;

use App\Projects\Helpers\RepositoryHelper;
use App\Projects\Project;

class TasksRepository extends RepositoryHelper
{
    public function __construct()
    {
        parent::__construct(new Task());
    }

    public function save($array)
    {
        $lowestPriority = Task::orderBy('priority', 'desc')->first();

        if (!$lowestPriority) {
            $lowestPriority = 0;
        }

        $array['priority'] = intval($lowestPriority->priority) + 10;

        return parent::save($array);
    }

    public function all()
    {
        return Task::with(['project'])->orderBy('priority')->get();
    }
}