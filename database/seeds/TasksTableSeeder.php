<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $project = \DB::table('projects')->inRandomOrder()->first();

            \DB::table('tasks')->insert([
                'name' => Str::random(10),
                'priority' => rand(0, 10),
                'project_id' => $project ? $project->id : null
            ]);
        }
    }
}
