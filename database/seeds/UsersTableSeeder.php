<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'Coalition Tech',
            'email' => 'admin@coalitiontech.com',
            'password' => \Illuminate\Support\Facades\Hash::make('password'),
        ]);
    }
}
