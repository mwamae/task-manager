## Coalition Technologies Task App

The application is an ordinary Laravel 6.x project. You will need PHP > 7.2 to run. 
 
To run the project, clone the repository/download (`git clone git@bitbucket.org:mwamae/task-manager.git`) the repo and follow the following steps:


Install Composer Dependencies by running `composer install` command. Then install NPM dependencies by running `npm install`.

Copy and update the env to reflect your environment

`cp .env.example .env`

 Generate app encryption key

`php artisan key:generate`

Migrate the database:

`php artisan migrate`

Seed the database to have the first user.

`php artisan db:seed`

You can then navigate to your browser and login using the following credentials:

email: admin@coalitiontech.com 
password: password